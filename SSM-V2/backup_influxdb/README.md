# Intro

A backup of data, users and roles is necessary for the InfluxDB stack of SSM.
The server used is ssm.cern.ch that had "disappear". Now is installed with a CENTOS 8 version from the CERN Repo.

## Backup Server (ssm2.cern.ch)

The backup server is ssm2.cern.ch. Has been configured with

    # dnf install nfs-utils

Once the installation is complete, start the nfs-server service, enable it to automatically start at system boot, and then verify its status using the systemctl commands:

    # systemctl start nfs-server.service
    # systemctl enable nfs-server.service
    # systemctl status nfs-server.service

Create the file systems to export or share on the NFS server. We will create the file system for the backups:

    # mkdir -p /home/backups

Then export the above file system in the NFS server /etc/exports configuration file to determine local physical file systems that are accessible to NFS clients.

    /home/backups                             172.26.104.244(rw,sync,no_root_squash)

To export the above file system, run the exportfs command with the -a flag that means export or unexport all directories, -r means reexport all directories, synchronizing /var/lib/nfs/etab with /etc/exports and files under /etc/exports.d, and -v enables verbose output.

    # exportfs -arv

To display the current export list, run the following command

    # exportfs -s

Next, the firewalld service running, you need to allow traffic to the necessary NFS services (mountd, nfs, rpc-bind) via the firewall, then reload the firewall rules to apply the changes, as follows.

    # firewall-cmd --permanent --add-service=nfs
    # firewall-cmd --permanent --add-service=rpc-bind
    # firewall-cmd --permanent --add-service=mountd # firewall-cmd --reload

## Backup Client (ssm.cern.ch)

Now on the client node(s), install the necessary packages to access NFS shares on the client systems. Run:

    # yum install nfs-utils nfs4-acl-tools

Run the showmount command to show mount information for the NFS server. The command should output the exported file system on the client

    # showmount -e  128.141.228.244
      Export list for 128.141.228.244:
      /home/backups 172.26.104.244

Next, create the local file system/directory for mounting the remote NFS file system and mount it as an ntf file systems

    # mkdir -p /root/tono/backup
    # mount -t nfs ssm2.cern.ch:/mnt/backups backup

Then confirm that the remote file system has been mounted by running the mount command and filter nfs mounts.

    # mount | grep nfs 

To enable the mount to persistent even after a system reboot, run the following command to enter the appropriate entry in the /etc/fstab.

    # echo "ssm2.cern.ch:/home/backups /root/tono/backup nfs defaults 0 0">>/etc/fstab 
    # cat /etc/fstab 

Lastly, test if NFS setup is working fine by creating a file on the server and check if the file can be seen in the client.

    # touch /home/backups/file_created_on_server.text [On NFS Server] 
    # ls -l /root/tono/backup/file_created_on_server.text [On NFS client] 

Scripts backup and restore are in the repo.

Provide the admin user credentials in the file .influx_admin:

    export INFLUX_USERNAME="admin"
    export INFLUX_PASSWORD="secret"

That's all.

Ref: https://influxdb.phys.ethz.ch/backup_restore.html
Ref: https://www.tecmint.com/install-nfs-server-on-centos-8/
