# pandoc --metadata date="`date +'%-d %B %Y'`"  -N -s -o render/ssm_report.pdf ssm_report.md
#pandoc -f markdown-implicit_figures --metadata date="`date +'%F'`"  -N -s -o render/ssm_report.pdf ssm_report.md
#pandoc -f markdown-implicit_figures --metadata date="`date +'%F'`"  -N -s -o render/ssm_report.docx ssm_report.md
pandoc -f docx -t markdown --extract-media . -o ssm_report.md ssm_report.docx
