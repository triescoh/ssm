# Render pdf

In order to generate the pdf, docx, etc. You must have pandoc: <https://pandoc.org/>

    pandoc  -N -s -o render/ssm_report.pdf ssm_report.md

To generate md from docx:

    pandoc -f docx -t markdown --extract-media ./ -o ssm_report.md ssm_report.docx

Install with brew in mac
