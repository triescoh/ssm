Version 1.01

Table of Contents

[1 Introduction 3](#introduction)

[2 Zabbix 3](#zabbix)

[2.1 Introduction 3](#introduction-1)

[2.2 Users of Zabbix 4](#users-of-zabbix)

[2.3 Role in SSM 4](#role-in-ssm)

[2.4 Present Performance of Zabbix in SSM
4](#present-performance-of-zabbix-in-ssm)

[2.4.1 Examples 4](#examples)

[3 TIG Stack 5](#tig-stack)

[3.1 History 5](#history)

[3.2 New system 2018 7](#new-system-2018)

[4 Present Architecture 8](#present-architecture)

[4.1 Hardware 8](#hardware)

[4.1.1 212 Building 9](#building)

[4.1.2 IT CERN Services 9](#it-cern-services)

[4.1.3 CERN Network 9](#cern-network)

[4.2 Software 9](#software)

[4.2.1 Zabbix Server 9](#zabbix-server)

[4.2.2 Telegraf 10](#telegraf)

[4.2.3 InfluxDB 11](#influxdb)

[4.2.4 Grafana 11](#grafana)

[4.2.5 IT Services 11](#it-services)

[4.2.6 Passive Hosts 11](#passive-hosts)

[4.2.7 SSM2 11](#ssm2)

[4.2.8 Zabbix Agent 11](#zabbix-agent)

[4.2.9 Zabbix Proxy 12](#zabbix-proxy)

1 Introduction
==============

SSM stands for Safety Systems Monitoring and the main goal is undertake
all the activities and processes to assure that the systems are
operating according to expectations and requirements defined.

Safety performance monitoring and measurement represents the means to
verify the safety performance of the systems supervised by SSM and to
validate the effectiveness of safety equipments.

SSM must provide the Operational Safety Systems with diagnostic data, so
that they can take corrective action in case of incident. Preferably
before it becomes apparent to the operations teams or the users of the
system.

SSM must provide the Operational Safety Systems team with long term
diagnostic data, so that post-mortem incident analysis can be made,
usage statistics can be extracted and so that trends can be analyzed for
future design considerations.

In a more general approach, the SSM system must assure that the Safety
Status of the Operational System is maintained and if not, provide the
necessary information to be repaired as soon as possible. This will
allow to effectively reduce the risk from an "unacceptable" to an
"acceptable" level.

2 Zabbix
========

2.1 Introduction
----------------

Zabbix is an enterprise-class open source distributed monitoring
solution.

Zabbix is the software that monitors numerous parameters of our network
and the health and integrity of servers, virtual machines, applications,
services, databases, websites and more. Zabbix uses a flexible
notification mechanism that allows users to configure e-mail based
alerts for virtually any event. This allows a fast reaction to server
problems. Zabbix offers excellent reporting and data visualization
features based on the stored data. This makes Zabbix ideal for capacity
planning.

Zabbix supports both polling and trapping. All Zabbix reports and
statistics, as well as configuration parameters, are accessed through a
web-based front-end. A web-based front-end ensures that the status of
your network and the health of your servers can be assessed from any
location. Properly configured, Zabbix can play an important role in
monitoring IT infrastructure. This is equally true for small
organizations with a few servers and for large companies with a
multitude of servers.

Zabbix is free of cost. Zabbix is written and distributed under the GPL
General Public License version 2. It means that its source code is
freely distributed and available for the general public.

Zabbix is one of the top tools available today for network monitoring
and management.

However, it comes with its own shortcomings as well and we're going to
go over some of the top alternatives to Zabbix below, but first lets see
some areas we can improve on.

Some of the possible shortcomings are:

-   Requires quite a bit if tuning to adapt to your environment. But
    once done, it can be a great tool.
-   The reporting tool could have been better, especially the reporting
    export feature.
-   Uses a lot more resources when compared to other similar monitoring
    tools.
-   Database has to be tuned well, otherwise it could large and unwieldy
    quickly.
-   SNMP monitoring is a drag on the resources, as a lot of connections
    are made and used for this task.
-   There is a steep learning curve associated with it.

2.2 Users of Zabbix
-------------------

Many organizations of different size around the world rely on Zabbix as
a primary monitoring platform. The Zabbix side is done with Zabbix 4.0.

2.3 Role in SSM
---------------

The main role is supervise the AAS, CSAM structure of servers, NAS, UPS,
virtual machines, applications, services, databases and websites related
with operation and safety equipment.

2.4 Present Performance of Zabbix in SSM
----------------------------------------

-   617 Hosts
-   13081 Items
-   4728 Triggers
-   88.4 Values updated per second
-   10 Proxies

### 2.4.1 Examples

-   Hosts could be a NAS or a Access Point Panel-PC.
-   Items could be Temperature or a free disk space in the NAS or
    available Memory or free space in C disk in one PC,
-   Trigger could be the Temperature in the NAS is too high or the free
    space in the hard disk of the Panel PC is less than 10%.
-   The system update 88 values each second from the present equipment
-   Since the system requirements is too high, some hosts are supervised
    by proxies that deliver later the data to the main server, SSM

3 TIG Stack
===========

3.1 History
-----------

TIS stand for Telegraf, InfluxDB and Grafana. The architecture was
deployed in 2018 and has been a success in the day by day operations.

The TIS was born because we needed to split the past architecture of
Zabbix in 2 sides, one special for Zabbix and the SNMP and other checks
done by TIS.

Before the TIG stack, Zabbix was doing all the work:

![](media/image1.png){width="5.833332239720035in"
height="3.2761297025371827in"}

In 2015 we moved to Zabbix 2.0 LTS and everything was working great, we
had set certain parameters in the Zabbix server configuration files that
all seem to be okay. The only issue we were running into is that the
database just did keep growing.

We did set our housekeeping retention to 60 days reducing from 1 year
retention data trying to reduce the database but the problem persisted.
We discovered that with the increment of external checks (scripts used
to get data from Oracle DBs) and SNMP Checks the system were slowing a
lot.

Effectively, each script requires starting a fork process by the Zabbix
server, running many scripts can decrease Zabbix performance a lot as is
explained in the [Zabbix
Documentation](https://www.zabbix.com/documentation/4.0/manual/config/items/itemtypes/external)

We decided to split the SSM work in 2 sides, one the principal
supervision system made with Zabbix and other with the scripts and SNMP
checks done by another stack.

We evaluated 3 popular stacks for doing the job:

-   LOGSTASH - ELASTICSEARCH - KIBANA
-   PROMETHEUS - NODE EXPORTER - GRAFANA
-   TELEGRAF - INFLUXDB -GRAFANA

![](media/image2.png){width="5.833333333333333in"
height="3.34706583552056in"}We did chose InfluxDB for its simplicity,
support and performance:

We did chose Grafana for several reasons, principally because was used a
lot at CERN, because its performance, integration with InfluxDB and
beautiful graphics.

A dashboard is a set of one or more panels organized and arranged into
one or more rows. Grafana ships with a variety of Panels.

Grafana makes it easy to construct the right queries, and customize the
display properties so that you can create the perfect dashboard for your
need. Each panel can interact with data from any configured Grafana Data
Source (currently Graphite, Prometheus, Elasticsearch, InfluxDB,
OpenTSDB, MySQL, PostgreSQL, Microsoft SQL Server and AWS Cloudwatch).

There is a lot of documentation and thousand of examples and templates
to get from the cloud to help to make good and useful dashboards.

Since 2018 more than 100 Dashboards have been done for the different
services

![](media/image3.png){width="5.833333333333333in"
height="3.122769028871391in"}

3.2 New system 2018
-------------------

![](media/image4.png){width="5.833333333333333in"
height="2.651062992125984in"}The path for the data is explained below
but the general architecture that was decided and it's working today is
here:

Adding the new path for the data we achieved several improvements in our
system:

-   Let's Zabbix do "Standard Zabbix".
-   Avoid ExternalChecks
-   Split channels.
-   Better maintenance and evolutions
-   Reuse all the old scripts

Finally, the system was split and has been working until today can be
seen in the Figure 5:

![](media/image5.png){width="5.833333333333333in"
height="3.358720472440945in"}

4 Present Architecture
======================

4.1 Hardware
------------

![](media/image6.png){width="5.833333333333333in"
height="4.157262685914261in"}

The hardware structure is based in 3 main systems.

### 4.1.1 212 Building

-   SSM Server. Proliant G9 with 3 TB in RAID 5.
-   SSM2 Server. Proliant G8 with 2 TB in RAID 5.

### 4.1.2 IT CERN Services

-   OpenStack VM with up to 10 SSM Proxy services.
-   Oracle Services.

### 4.1.3 CERN Network

-   Operators and clients for data
-   SNMP Clients. (NAS, Cameras, UPS, etc.)
-   Linux Clients. Basically servers.
-   Windows Clients. Panel PC, SUSI servers, etc.
-   NAS Clients with special requirements.

4.2 Software
------------

![](media/image7.png){width="5.833333333333333in"
height="3.8774507874015747in"} The software structure is based in the
following subsystems running with CERN CENTOS 7 for the own servers:

### 4.2.1 Zabbix Server

Zabbix server is the central process of Zabbix software.

The server performs the polling and trapping of data, it calculates
triggers, sends notifications to users. It is the central component to
which Zabbix agents and proxies report data on availability and
integrity of systems. The server can itself remotely check networked
services (such as web servers or ping services) using simple service
checks.

The server is the central repository in which all configuration,
statistical and operational data is stored, and it is the entity in
Zabbix that will actively alert administrators when problems arise in
any of the monitored systems.

The functioning of a basic Zabbix server is broken into three distinct
components; they are:

-   Zabbix server 4.0 running in CENTOS 7 (CERN)
-   Web front-end (Zabbix 4.0)
-   Database storage. MySQL 5.5 running in own server.

All of the configuration information for Zabbix is stored in the
database, which both the server and the web front-end interact with.

For example, when you create a new item using the web front-end (or API)
it is added to the items table in the database. Then, about once a
minute Zabbix server will query the items table for a list of the items
which are active that is then stored in a cache within the Zabbix
server. This is why it can take up to two minutes for any changes made
in Zabbix front-end to show up in the latest data section.

The main tasks of the service are:

-   Store metric values
-   Get data from devices by pulling
-   Get data from devices by agents
-   Get data from proxies
-   Send data to Grafana
-   Send alarms to e-groups
-   Make basic supervision graphics

### 4.2.2 Telegraf

Telegraf is a plugin-driven server agent for collecting & reporting
metrics, and is with the Zabbix Agent the first piece of the SSM stack.
Telegraf has plugins to source a variety of metrics directly from the
system it's running on, pull metrics from third party APIs, SNMP Clients
like UPS, Cameras, NAS, etc.

It also has output plugins to send metrics to a variety of other
datastores, services, and message queues, including InfluxDB Graphite,
OpenTSDB, Datadog, Librato, Kafka, MQTT, NSQ, and many others.

Telegraf's plugin architecture supports collection of metrics from 100+
popular services right out of the box.

In our configuration the data is written to the InfluxDB Database.

The main tasks of the service are:

-   Get values for SNMP clients
-   Get values for services in SSM Host
-   Get values from Oracle DB with shell scripts
-   Write values to InfluxDB

### 4.2.3 InfluxDB

InfluxDB is a high performance Time Series Database. It can store
hundreds of thousands of points per second. The InfluxDB SQL-like query
language was built specifically for time series and is used in special
cases.

Basically is used for store the values received from telegraf clients
and serve the data to Grafana clients.

The main tasks of the service are:

-   Get values from Telegraf
-   Store metric values
-   Give the data to clients

### 4.2.4 Grafana

Grafana is a open source visualization and analytic software. It allows
you to query, visualize, alert on, and explore our metrics no matter
where they are stored. In our configuration reads and write from Zabbix,
Oracle DB, MySQL DB, MSSQL DB and InfluxDB sources. It provides us with
tools to turn our time-series database (TSDB) InfluxDB data into
beautiful graphs and visualizations.

The main tasks of the service are:

-   Get values from InfluxDB
-   Show dashboards
-   Send alarms defined by users
-   Get values from Zabbix
-   Acknowledge alarms to Zabbix

### 4.2.5 IT Services

In the IT services the principal source of data are Oracle DB used by
SUSI devices, Adams Service, etc.

### 4.2.6 Passive Hosts

In this category we find equipment that only can be checked by simple
protocols like ping, snmp, etc.

### 4.2.7 SSM2

The main work of this server is being a backup and a urgent replacement
in case of problems with the main server. Also is used to maintain and
do the backups to the central services at CERN to avoid charging this
task to the main server.

### 4.2.8 Zabbix Agent

Zabbix agents are deployed on monitoring targets to actively monitor
local resources and applications and report the gathered data to Zabbix
server. There are two types of agents available: the Zabbix agent
(lightweight, supported on many platforms, written in C) and the Zabbix
agent 2 (extra-flexible, easily extendable with plugins, written in Go).

The main tasks are:

-   Get data from own host
-   Send data actively to proxy or server
-   Send data by request to proxy or server

### 4.2.9 Zabbix Proxy

Zabbix proxy are collecting performance and availability data on behalf
of Zabbix server. The proxy is an optional part of Zabbix deployment;
however, it is very beneficial to distribute the load of our single
Zabbix server.

The main tasks are:

-   Get data from Zabbix Agents
-   Get configuration from Zabbix Server
-   Send data actively server
-   Send data by request to server
-   Get data from own host
