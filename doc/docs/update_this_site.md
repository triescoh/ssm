### Update this site

Git clone the repo from [Gitlab CERN](https://gitlab.cern.ch/triescoh/ssm) and modify the doc. In the doc directory:

```bash
mkdocs build
```

In lxplus:

```bash
rsync -av ~/eos/Documents/SSM/doc/site/ /eos/project/s/ssm/www/
```

### Todo

* Update automatically the EOS repo of SSM from Gitlab
